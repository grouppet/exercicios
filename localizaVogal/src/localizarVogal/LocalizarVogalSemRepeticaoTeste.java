package localizarVogal;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class LocalizarVogalSemRepeticaoTeste {
	
	@Test
	public void testLetraNaoRepetidaConsoanteAntes() {
		
		Letra buscaVoga5 = new Letra("Eba");
		Letra buscaVogal = new Letra("aAbBABacafe");
		Letra buscaVogal4 = new Letra("CubaBuBAchheefi");
		Letra buscaVogal2 = new Letra("aAboBuBacaefe");
		Letra buscaVogal3 = new Letra("aCubaBBAchhefe");
		
		assertEquals('a', LocalizarVogalSemRepeticao.buscaVogal(buscaVoga5));
		assertEquals('e', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal));
		assertEquals('i', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal4));
		assertEquals('o', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal2));
		assertEquals('u', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal3));
		
	}
	
	@Test
	public void testNenhumaVogalUnicaEncontrada() {
		
		Letra buscaVogal1 = new Letra("aAbeBABacafe");
		Letra buscaVogal2 = new Letra("");
		Letra buscaVogal3 = null;
		Letra buscaVogal4 = new Letra("ab");
		
		assertEquals('0', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal1));
		assertEquals('0', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal2));
		assertEquals('0', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal3));
		assertEquals('0', LocalizarVogalSemRepeticao.buscaVogal(buscaVogal4));
	}

}
