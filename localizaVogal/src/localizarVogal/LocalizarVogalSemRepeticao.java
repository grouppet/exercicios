package localizarVogal;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class LocalizarVogalSemRepeticao {
	
	static String vogais = "[aeiou]";
	
	public static char buscaVogal(Stream palavra) {

		if (palavra == null || palavra.toString().trim().equals("")) {
			return '0';
		}
		
		Map<Character, Integer> contagemVogais = new LinkedHashMap<Character, Integer>();
		Map<Character, Integer> contagemLetras = new LinkedHashMap<Character, Integer>();
		
		char letraAnterior = ' ';
		char letraAtual = ' ';
		char proximaLetra = ' ';
		
		while (palavra.hasNext()) {
			
			letraAnterior = letraAtual;
			letraAtual = proximaLetra;
			proximaLetra = palavra.getNext();
			
			//Guarda todas as vogais que a letra anterior � uma consoante e a antecessora � uma vogal no Map, contabilizando o quantidade de vezes que a mesma esta presente na string
			if(!Pattern.matches(getVogais(), String.valueOf(letraAtual)) && 
				Pattern.matches(getVogais(), String.valueOf(letraAnterior)) &&
				Pattern.matches(getVogais(), String.valueOf(proximaLetra))) {
				
				if(contagemVogais.get(proximaLetra)!=null) {
					contagemVogais.put(proximaLetra, contagemVogais.get(proximaLetra) + 1);
				}else if(contagemLetras.get(proximaLetra)!=null) {
					contagemVogais.put(proximaLetra, 2);
				}else {
					contagemVogais.put(proximaLetra, 1);
				}

			}
			
			if(contagemLetras.get(proximaLetra)!=null) {
				contagemLetras.put(proximaLetra, contagemLetras.get(proximaLetra) + 1);
			}else {
				contagemLetras.put(proximaLetra, 1);
			}
			
		}

		Map.Entry<Character, Integer> resposta = contagemVogais.entrySet().stream().filter(entry -> entry.getValue()==1).findFirst().orElse(null);
		
		return resposta==null?'0':resposta.getKey();

	}

	public static String getVogais() {
		return vogais;
	}
	

}
