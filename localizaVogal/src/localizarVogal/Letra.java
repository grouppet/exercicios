package localizarVogal;

public class Letra implements Stream{
	
	private String texto = null;
	private Integer posicao = null;
	
			
	public Letra(String texto) {
		

			if(texto !=null && !texto.trim().equals("")) {
				this.texto = texto.toLowerCase();
			}
			
			this.posicao = 0;
	
	}
	@Override
	public char getNext() {
		if (hasNext()) {
			return this.texto.charAt(posicao++);
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean hasNext() {
		if (this.texto == null || this.texto.length() == 0) {
			return false;
		}
		else {
			if (this.posicao >= this.texto.length()) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	

}
