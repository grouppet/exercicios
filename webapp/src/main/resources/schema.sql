CREATE TABLE IF NOT EXISTS time_tb
(
	id integer not null IDENTITY,
	nome varchar(255) not null,
	primary key(id)

);

CREATE TABLE IF NOT EXISTS campanha_tb
(
   id integer not null IDENTITY,
   nome varchar(255) not null,
   dt_ini date not null,
   dt_fim date not null,
   id_time integer not null,
   primary key(id),
   FOREIGN KEY(id_time)
     REFERENCES time_tb(id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS cliente_campanha_tb
(
	id integer not null IDENTITY,
	id_cliente integer not null,
	id_campanha  integer not null,
	primary key(id),
    FOREIGN KEY(id_campanha)
     REFERENCES campanha_tb(id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
);