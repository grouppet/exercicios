package br.com.categoria.webapp.model;

import java.util.Calendar;
import java.util.Date;

public class Campanha {
	
	private Integer id;
	private String nome;
	private Date dtIni;
	private Date dtFim;
	private Integer idTime;
	
	public Campanha(Integer id, String nome, Date dtIni, Date dtFim, Integer idTime) {
		this.id = id;
		this.nome = nome;
		this.dtIni = dtIni;
		this.dtFim = dtFim;
		this.idTime = idTime;
	}
	
	public Campanha(Integer id, Date dtFim) {
		this.id = id;
		this.dtFim = dtFim;
	}
	
	public Campanha() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDtIni() {
		return dtIni;
	}
	public void setDtIni(Date dtIni) {
		this.dtIni = dtIni;
	}
	public Date getDtFim() {
		return dtFim;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public Integer getIdTime() {
		return idTime;
	}

	public void setIdTime(Integer idTime) {
		this.idTime = idTime;
	}
	
}
