package br.com.categoria.webapp.dao;

import br.com.categoria.webapp.model.Cliente;

public interface AssociaClienteDAO {

	public int inserir(Integer idCampanha, Integer idCliente);
	public int associarTodasCampanhas(Cliente cliente);
}
