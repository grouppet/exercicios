package br.com.categoria.webapp.jms;

import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class Consumidor {

    private static Logger log = LoggerFactory.getLogger(Consumidor.class);

    @JmsListener(destination = "ordem")
    public void receiveMessage(@Payload Mensagem mensagem,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {
        log.info("received <" + mensagem + ">");
        log.info("- - - - - - - - - - - - - - - - - - - - - - - -");
        log.info("headers: " + headers);
        log.info("message: " + message);
        log.info("session: " + session);
        log.info("- - - - - - - - - - - - - - - - - - - - - - - -");
    }

}
