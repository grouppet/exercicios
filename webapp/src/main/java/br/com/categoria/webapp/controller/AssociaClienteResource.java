package br.com.categoria.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.categoria.webapp.dao.AssociaClienteDAO;
import br.com.categoria.webapp.model.Campanha;
import br.com.categoria.webapp.model.Cliente;

@RestController
public class AssociaClienteResource {
	
	@Autowired
	AssociaClienteDAO associaDao;
	
	
	// ATUALIZAR
		@PutMapping("/clientes")
		public Cliente atualizar(@RequestBody Cliente cliente) {

			if (cliente !=null && associaDao.associarTodasCampanhas(cliente)>0) {
				
				System.out.println("Associado campanhas ao id: " + cliente.getId());
				
			}else {
				System.out.println("Nenhuma campanha associada ao id: " + cliente.getId());
			}

			return null;
		}
}
