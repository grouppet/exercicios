package br.com.categoria.webapp.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Direcionador {

    private static Logger log = LoggerFactory.getLogger(Direcionador.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public void send(Mensagem myMessage) {
        log.info("sending with convertAndSend() to queue <" + myMessage + ">");
        jmsTemplate.convertAndSend("ordem", myMessage);
    }
}
