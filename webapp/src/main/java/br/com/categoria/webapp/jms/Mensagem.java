package br.com.categoria.webapp.jms;

import java.io.Serializable;


public class Mensagem  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idCliente;

	public Mensagem() {
		
	}
	
	public Mensagem(int idCliente) {
		super();
		this.idCliente = idCliente;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	@Override
    public String toString() {
        return "{" +
                "idCliente:'" + idCliente +
                '}';
    }

	
 	
}
