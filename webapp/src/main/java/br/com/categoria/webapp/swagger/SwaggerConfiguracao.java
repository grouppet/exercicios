package br.com.categoria.webapp.swagger;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguracao {
	
	public static final Contact CONTATO = new Contact(
		      "Felipe Andrade", "", "lipe_460@hotmail.com");
		  
		  public static final ApiInfo API_INFO = new ApiInfo(
		      "API Campanha", "Descrição", "1.0",
		      "urn:tos", CONTATO, 
		      "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");

		  private static final Set<String> PRODUTORES_E_CONSUMIDORES = 
		      new HashSet<String>(Arrays.asList("application/json",
		          "application/xml"));

		  @Bean
		  public Docket api() {
		    return new Docket(DocumentationType.SWAGGER_2)
		        .apiInfo(API_INFO)
		        .produces(PRODUTORES_E_CONSUMIDORES)
		        .consumes(PRODUTORES_E_CONSUMIDORES);
		  }
}
