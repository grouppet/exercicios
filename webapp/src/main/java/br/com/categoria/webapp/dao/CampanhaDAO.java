package br.com.categoria.webapp.dao;

import java.util.Date;
import java.util.List;

import br.com.categoria.webapp.model.Campanha;

public interface CampanhaDAO {
	
	public int atualizar(Campanha campanha);
	public int inserir(Campanha campanha);
	public int deletar(Integer id);
	public Campanha consultar(Integer id);
	public List<Campanha> listar();
	
	
	public List<Campanha> listarCampanhasPorVigencia(Campanha campanha);
	public int atualizarCampanha(Campanha campanha);

}
