package br.com.categoria.webapp.test;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import br.com.categoria.webapp.model.Campanha;

public class CampanhaClienteTeste {
	
	public void getCampanhaPorId() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8090/campanhas/{id}";
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Campanha> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Campanha.class, 2);
		Campanha campanha = responseEntity.getBody();
		System.out.println("Id:" + campanha.getId() + ", Nome:" + campanha.getNome() + ", Data INI:"
				+ campanha.getDtIni() + ", Data FIM:" + campanha.getDtFim() + ", ID TIME:" + campanha.getIdTime());
	}

	public void getTodasCampanhas() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8090/campanhas";
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<Campanha[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Campanha[].class);
		Campanha[] campanhas = responseEntity.getBody();
		for (Campanha campanha : campanhas) {
			System.out.println("Id:" + campanha.getId() + ", Nome:" + campanha.getNome() + ", Data INI:"
					+ campanha.getDtIni() + ", Data FIM:" + campanha.getDtFim() + ", ID TIME:" + campanha.getIdTime());
		}
	}

	public void cadastrarCampanha() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8090/campanhas";
		Campanha campanha = new Campanha();
		campanha.setNome("Campanha 10");
	
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dataIni = new Date();
		Date dataFim = new Date();
		try {
			dataIni = format.parse("2017-10-01");
			dataFim = format.parse("2017-10-03");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		campanha.setDtIni(dataIni);
		campanha.setDtFim(dataFim);
		campanha.setIdTime(1);

		HttpEntity<Campanha> requestEntity = new HttpEntity<Campanha>(campanha, headers);
		URI uri = restTemplate.postForLocation(url, requestEntity);

	}

	public void atualizarCampanha() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8090/campanhas";

		Campanha campanha = new Campanha();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dataIni = new Date();
		Date dataFim = new Date();
		try {
			dataIni = format.parse("2017-10-01");
			dataFim = format.parse("2018-10-03");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		campanha.setId(2);
		campanha.setNome("Campanha Editada");
		campanha.setDtIni(dataIni);
		campanha.setDtFim(dataFim);
		campanha.setIdTime(2);
		
		HttpEntity<Campanha> requestEntity = new HttpEntity<Campanha>(campanha, headers);
		restTemplate.put(url, requestEntity);
	}

	public void apagarCapanha() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8090/campanhas/{id}";
		HttpEntity<Campanha> requestEntity = new HttpEntity<Campanha>(headers);
		restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, 1);
	}

	public static void main(String args[]) {
		CampanhaClienteTeste util = new CampanhaClienteTeste();
		util.getCampanhaPorId(); //Teste pesquisa por 1 campanha
		util.cadastrarCampanha();//Teste cadastrar campanha atendendo requisitos das datas para campanhas no mesmo periodo de vigência
		util.atualizarCampanha();//Atualizar uma campanha
		util.apagarCapanha();//Apagar uma campanha
		util.getTodasCampanhas();//Listar todas as campanhas vigentes a partir da data corrente
	}
	


}
