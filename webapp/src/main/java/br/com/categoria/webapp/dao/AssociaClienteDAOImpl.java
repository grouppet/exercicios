package br.com.categoria.webapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import br.com.categoria.webapp.model.Cliente;

@Repository("associaDao")
public class AssociaClienteDAOImpl implements AssociaClienteDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int inserir(Integer idCampanha, Integer idCliente) {
		return jdbcTemplate.update("insert into cliente_campanha_tb (id_campanha, id_cliente) " + "values(?, ?)",
				new Object[] { idCampanha, idCliente });
	}
	
	public int associarTodasCampanhas(Cliente cliente) {
		
		
	
		
		return jdbcTemplate.update("insert into  cliente_campanha_tb (id_cliente, id_campanha) " + 
				"SELECT ?, c.id FROM CAMPANHA_TB c " + 
				"left join cliente_campanha_tb cc on cc.id_campanha = c.id " + 
				"where cc.id is null and c.id_time = ? and getDate() between c.dt_ini and c.dt_fim",
				new Object[] { cliente.getId(), cliente.getIdTime() });
	}
	

}
