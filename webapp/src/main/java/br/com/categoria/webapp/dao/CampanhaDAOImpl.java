package br.com.categoria.webapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import br.com.categoria.webapp.model.Campanha;

@Repository("campanhaDao")
public class CampanhaDAOImpl implements CampanhaDAO{
	
	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	class CampanhaRowMapper implements RowMapper<Campanha> {
		@Override
		public Campanha mapRow(ResultSet rs, int rowNum) throws SQLException {
			Campanha campanha = new Campanha();
			campanha.setId(rs.getInt("id"));
			campanha.setNome(rs.getString("nome"));
			campanha.setDtIni(rs.getDate("dt_ini"));
			campanha.setDtFim(rs.getDate("dt_fim"));
			campanha.setIdTime(rs.getInt("id_time"));
			return campanha;
		}

	}
	
	
	public List<Campanha> listar() {
		return jdbcTemplate.query("select * from campanha_tb where getdate() between dt_ini and dt_fim;", new CampanhaRowMapper());
	}

	public Campanha consultar(Integer id) {
		return jdbcTemplate.queryForObject("select * from campanha_tb where id=?", new Object[] { id },
				new BeanPropertyRowMapper<Campanha>(Campanha.class));
	}

	public int deletar(Integer id) {
		return jdbcTemplate.update("delete from campanha_tb where id=?", new Object[] { id });
	}

	public int inserir(Campanha campanha) {
		return jdbcTemplate.update("insert into campanha_tb (nome, dt_ini, dt_fim, id_time) " + "values(?, ?, ?, ?)",
				new Object[] { campanha.getNome(), campanha.getDtIni(), campanha.getDtFim(), campanha.getIdTime() });
	}

	public int atualizar(Campanha campanha) {
		return jdbcTemplate.update("update campanha_tb " + " set dt_fim = ? " + " where id = ?",
				new Object[] { campanha.getDtFim(), campanha.getId() });
	}
	
	public int atualizarCampanha(Campanha campanha) {
		return jdbcTemplate.update("update campanha_tb " + " set dt_ini = ?, dt_fim = ?, nome = ?, id_time = ?" + " where id = ?",
				new Object[] { campanha.getDtIni(), campanha.getDtFim(), campanha.getNome(), campanha.getIdTime(), campanha.getId() });
	}

	public List<Campanha> listarCampanhasPorVigencia(Campanha campanha) {
		java.sql.Date sqlDataIni = new java.sql.Date(campanha.getDtIni().getTime());
		java.sql.Date sqlDataFim = new java.sql.Date(campanha.getDtFim().getTime());
		return jdbcTemplate.query("select * from campanha_tb where dt_ini between \'"+ sqlDataIni+"\' and \'"+sqlDataFim+"\'", new CampanhaRowMapper());
	}
}
