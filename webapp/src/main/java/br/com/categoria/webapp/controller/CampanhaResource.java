package br.com.categoria.webapp.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.categoria.webapp.dao.CampanhaDAO;
import br.com.categoria.webapp.jms.Mensagem;
import br.com.categoria.webapp.jms.Direcionador;
import br.com.categoria.webapp.model.Campanha;

@RestController
public class CampanhaResource {

	@Autowired
	CampanhaDAO campanhaDao;
	
	 private static Logger log = LoggerFactory.getLogger(CampanhaResource.class);

	    @Autowired
	    private Direcionador orderSender;

	public CampanhaResource() {
	
	}

	// INSERIR
	@PostMapping("/campanhas")
	public Campanha inserir(@RequestBody Campanha campanha) {

		List<Campanha> retorno = campanhaDao.listarCampanhasPorVigencia(campanha);

		Map<Integer, Date> collect = retorno.stream().collect(Collectors.toMap(Campanha::getId, Campanha::getDtFim));

		Date data = campanha.getDtFim();

		while (verificarRepeticaoDataFim(collect, data)) {

			collect.entrySet().forEach(entry -> {

				Date dataMap = entry.getValue();
				Calendar c = Calendar.getInstance();
				c.setTime(dataMap);
				c.add(Calendar.DATE, 1);

				Date output = c.getTime();
				collect.put(entry.getKey(), output);

			});

		}

		if (collect != null && !collect.isEmpty()) {
			collect.entrySet().forEach(entry -> 
				{
				campanhaDao.atualizar(new Campanha(entry.getKey(), entry.getValue()));
				Mensagem mensagem = new Mensagem(entry.getKey());
				orderSender.send(mensagem);
				}
			);
			
            
		}

		campanhaDao.inserir(campanha);

		System.out.println("Campanha Salva com sucesso! campanha! "
				+ ((collect != null && !collect.isEmpty()) ? "Também foram atualizadas campanhas existentes!" : ""));

		return campanha;
	}

	// CONSULTAR TODAS
	@GetMapping("/campanhas")
	public ResponseEntity<List<Campanha>> listar() {


		List<Campanha> retorno = campanhaDao.listar();

		if (retorno == null || retorno.size() == 0) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Campanha>>(new ArrayList<Campanha>(retorno), HttpStatus.OK);

	}

	boolean validacao = false;

	public boolean verificarRepeticaoDataFim(Map<Integer, Date> collect, Date data) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date dataAnalise = cal.getTime();
		validacao = false;
		if (collect == null || dataAnalise == null || collect.isEmpty()) {
			return validacao;
		}
		collect.entrySet().stream().forEach(camp -> {

			if (camp.getValue().equals(dataAnalise)) {

				validacao = true;
			}

		});

		return validacao;
	}

	// CONSULTAR 1
	@GetMapping("/campanhas/{id}")
	public ResponseEntity<Campanha> consultar(@PathVariable("id") Integer id) {
		Campanha campanha = campanhaDao.consultar(id);

		if (campanha == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Campanha>(campanha, HttpStatus.OK);
	}

	// DELETAR
	@DeleteMapping("/campanhas/{id}")
	public void deletar(@PathVariable("id") Integer id) {
		campanhaDao.deletar(id);
		System.out.println("Campanha: " + id + " foi apagada!");
	}

	// ATUALIZAR
	@PutMapping("/campanhas")
	public Campanha atualizar(@RequestBody Campanha campanha) {

		if (campanha!=null) {
			campanhaDao.atualizarCampanha(campanha);
			System.out.println("Atualizado a campanha: " + campanha.getId());
		}

		return null;
	}
	
}
