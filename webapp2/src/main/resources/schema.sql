CREATE TABLE IF NOT EXISTS time_tb
(
	id integer not null IDENTITY,
	nome varchar(255) not null,
	primary key(id)

);

CREATE TABLE IF NOT EXISTS cliente_tb
(
   id integer not null IDENTITY,
   nome varchar(255) not null,
   dt_nasc date not null,
   email varchar(255) not null,
   id_time integer not null,
   primary key(id),
   FOREIGN KEY(id_time)
     REFERENCES time_tb(id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
	
);