package br.com.categoria.webapp.dao;

import br.com.categoria.webapp.model.Cliente;

public interface ClienteDAO {
	
	public boolean validaEmail(String email);
	
	public int inserir(Cliente cliente);

	public Cliente retornaIdCliente(String email);
	
}
