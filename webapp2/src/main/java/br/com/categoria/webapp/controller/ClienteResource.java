package br.com.categoria.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.categoria.webapp.dao.ClienteDAO;
import br.com.categoria.webapp.model.Cliente;

@RestController
public class ClienteResource {
	
	@Autowired
	ClienteDAO clienteDAO;

	// INSERIR
		@PostMapping("/clientes")
		public Cliente inserir(@RequestBody Cliente cliente) {
			Cliente c = cliente;
			if(cliente!=null && cliente.getEmail()!=null) {
				
				//Verifica se e-mail não existe
				if(clienteDAO.validaEmail(cliente.getEmail())) {
					clienteDAO.inserir(cliente);
					c = clienteDAO.retornaIdCliente(cliente.getEmail());
				}
				
				//Chamada para vicular as campanhas vigentes e não associdadas ao cliente
				try {
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					RestTemplate restTemplate = new RestTemplate();
					String url = "http://localhost:8090/clientes";
					HttpEntity<Cliente> requestEntity = new HttpEntity<Cliente>(c, headers);
					restTemplate.put(url, requestEntity);
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
			}
			
			return c;
		}
	
}
