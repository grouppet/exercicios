package br.com.categoria.webapp.model;

import java.util.Date;

public class Cliente {

	private Integer id;
	private String email;
	private String nome;
	private Date dtNasc;
	private Integer idTime;
	
	public Cliente() {
		
	}
	
	public Cliente(String email, String nome, Date dtNasc, Integer idTime) {
		super();
		this.email = email;
		this.nome = nome;
		this.dtNasc = dtNasc;
		this.idTime = idTime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDtNasc() {
		return dtNasc;
	}
	public void setDtNasc(Date dtNasc) {
		this.dtNasc = dtNasc;
	}
	public Integer getIdTime() {
		return idTime;
	}
	public void setIdTime(Integer idTime) {
		this.idTime = idTime;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
}
