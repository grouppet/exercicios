package br.com.categoria.webapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import br.com.categoria.webapp.model.Cliente;
@Repository
public class ClienteDAOImpl implements ClienteDAO{
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	class ClienteRowMapper implements RowMapper<Cliente> {
		@Override
		public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
			Cliente cliente = new Cliente();
			cliente.setId(rs.getInt("id"));
			cliente.setNome(rs.getString("nome"));
			cliente.setDtNasc(rs.getDate("dt_nasc"));
			cliente.setEmail(rs.getString("email"));
			cliente.setIdTime(rs.getInt("id_time"));
			return cliente;
		}

	}
	
	public boolean validaEmail(String email) {
		
		  
		
		if(jdbcTemplate.query("select * from cliente_tb where email=?", new Object[] { email },
				new BeanPropertyRowMapper<Cliente>(Cliente.class)).isEmpty()) {
			return true;
		}else {
			return false;
		}
		
	}
		
	public Cliente retornaIdCliente(String id) {
		return jdbcTemplate.queryForObject("select * from cliente_tb where email=?", new Object[] { id },
				new BeanPropertyRowMapper<Cliente>(Cliente.class));
	}
	
	public int inserir(Cliente cliente) {
		return jdbcTemplate.update("insert into cliente_tb (nome, dt_nasc, email, id_time) " + "values(?, ?, ?, ?)",
				new Object[] { cliente.getNome(), cliente.getDtNasc(), cliente.getEmail(), cliente.getIdTime() });
	}

}
