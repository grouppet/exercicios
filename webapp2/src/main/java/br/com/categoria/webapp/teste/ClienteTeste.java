package br.com.categoria.webapp.teste;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import br.com.categoria.webapp.model.Cliente;

public class ClienteTeste {

	public void cadastrarCliente() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/clientes";
		Cliente cliente = new Cliente();
		cliente.setNome("João");
		cliente.setEmail("alessandro@hotmail.com");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dataNasc = new Date();

		try {
			dataNasc = format.parse("1989-02-01");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		cliente.setDtNasc(dataNasc);
		cliente.setIdTime(1);

		HttpEntity<Cliente> requestEntity = new HttpEntity<Cliente>(cliente, headers);
		URI uri = restTemplate.postForLocation(url, requestEntity);

	}
	
	public void clienteExistente() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/clientes";
		Cliente cliente = new Cliente();
		cliente.setEmail("ana@hotmail.com");
		
		HttpEntity<Cliente> requestEntity = new HttpEntity<Cliente>(cliente, headers);
		URI uri = restTemplate.postForLocation(url, requestEntity);

	}
	
	public static void main(String args[]) {
		ClienteTeste util = new ClienteTeste();
		util.cadastrarCliente(); //Teste Cadastro Novo Usuário
		util.clienteExistente(); //Teste Email já existente
		
	}
	
}
